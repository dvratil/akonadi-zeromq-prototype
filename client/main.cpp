#include "../common.h"

#include <memory>
#include <iostream>

#include <unistd.h>
#include <cstring>

int main(int argc, char **argv)
{
    std::unique_ptr<void, ZmqContextDeleter> ctx{zmq_ctx_new()};

    std::unique_ptr<void, ZmqSocketDeleter> socket{zmq_socket(ctx.get(), ZMQ_DEALER)};

    char identity[100];
    std::sprintf(identity, "Client%d", getpid());

    zmq_setsockopt(socket.get(), ZMQ_IDENTITY, identity, strlen(identity));
    zmq_connect(socket.get(), "tcp://localhost:5570");

    sendDelimiter(socket.get(), ZMQ_SNDMORE);

    char hello[] = "hello";
    zmq_msg_t helloMsg;
    zmq_msg_init_data(&helloMsg, hello, 5, nullptr, nullptr);
    zmq_msg_send(&helloMsg, socket.get(), 0);

    while (true) {
        {
            std::string input;
            std::cout << "<< ";
            std::cin >> input;

            sendDelimiter(socket.get(), ZMQ_SNDMORE);

            zmq_msg_t msg;
            zmq_msg_init_data(&msg, const_cast<char *>(input.c_str()), input.size(), nullptr, nullptr);
            zmq_msg_send(&msg, socket.get(), 0);
        }

        {
            // Delimiter
            readDelimiter(socket.get());

            // Data
            zmq_msg_t msg;
            zmq_msg_init(&msg);
            zmq_msg_recv(&msg, socket.get(), 0);
            std::string str{static_cast<const char *>(zmq_msg_data(&msg)), zmq_msg_size(&msg)};
            std::cout << ">> " << str << std::endl;
        }
    }

    return 0;
}
