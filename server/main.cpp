#include "../common.h"

#include <memory>
#include <future>
#include <unordered_map>
#include <string_view>
#include <iostream>
#include <vector>
#include <cassert>

class Connection
{
public:
    explicit Connection(void *zmq_ctx, std::string routingId)
        : mId{std::move(routingId)}
    {
        mSocket = std::unique_ptr<void, ZmqSocketDeleter>{zmq_socket(zmq_ctx, ZMQ_DEALER)};
        zmq_setsockopt(mSocket.get(), ZMQ_IDENTITY, mId.c_str(), mId.size());
        if (zmq_connect(mSocket.get(), "inproc://backend") != 0) {
            const auto errnosnap = errno;
            std::cerr << "[" << mId << "] ERROR: Failed to connect to inproc://backend: "
                      << zmq_strerror(errnosnap) << " (" << errnosnap << ")";
            return;
        };

        std::cout << "[" << mId << "] CONNHANDLER CREATED" << std::endl;
    }

    const std::string &id() const
    {
        return mId;
    }

    void run()
    {
        while (true) {
            // delimiter
            readDelimiter(mSocket.get());

            // data
            zmq_msg_t msg;
            zmq_msg_init(&msg);
            zmq_msg_recv(&msg, mSocket.get(), 0);

            const std::string data{static_cast<const char *>(zmq_msg_data(&msg)), zmq_msg_size(&msg)};
            std::cout << "[" << mId << "] RECV " << data << std::endl;


            sendDelimiter(mSocket.get(), ZMQ_SNDMORE);

            zmq_msg_t respMsg;
            char ok[] = "OK";
            zmq_msg_init_data(&respMsg, ok, 2, nullptr, nullptr);
            zmq_msg_send(&respMsg, mSocket.get(), 0);
        }
    }

private:
    std::string mId;
    std::unique_ptr<void, ZmqSocketDeleter> mSocket;
};


class Server
{
public:
    Server()
        : mCtx{zmq_ctx_new()}
        , mFrontend{zmq_socket(mCtx.get(), ZMQ_ROUTER)}
        , mBackend{zmq_socket(mCtx.get(), ZMQ_ROUTER)}
    {
        zmq_bind(mFrontend.get(), "tcp://*:5570");
        zmq_bind(mBackend.get(), "inproc://backend");
        zmq_socket_monitor(mFrontend.get(), "inproc://frontend-monitor", ZMQ_EVENT_ALL);
        mMonitorThread = std::thread{&Server::monitor, this};
    }

    void monitor()
    {
        std::unique_ptr<void, ZmqSocketDeleter> monitorSocket{zmq_socket(mCtx.get(), ZMQ_PAIR)};
        zmq_connect(monitorSocket.get(), "inproc://frontend-monitor");

        while (true) {
            // Event type and value
            zmq_msg_t msg;
            zmq_msg_init(&msg);
            zmq_msg_recv(&msg, monitorSocket.get(), 0);

            struct Data {
                uint16_t eventType;
                uint32_t value;
            };

            const auto [eventType, value] =  *static_cast<Data *>(zmq_msg_data(&msg));

            // Event data
            zmq_msg_init(&msg);
            zmq_msg_recv(&msg, monitorSocket.get(), 0);
            const std::string data{static_cast<char *>(zmq_msg_data(&msg)), zmq_msg_size(&msg)};

            switch (eventType) {
            case ZMQ_EVENT_CONNECTED:
                std::cout << "[MON] New connection on FD " << value << std::endl;
                break;
            case ZMQ_EVENT_DISCONNECTED:
                std::cout << "[MON] Disconnected FD " << value << std::endl;
                break;
            }
        }
    }


    void run()
    {
        std::cout << "[SRV] Waiting for messages..." << std::endl;
        while (true) {
            zmq_pollitem_t pollItems[2] = {
                { mFrontend.get(), 0, ZMQ_POLLIN, 0 },
                { mBackend.get(), 0, ZMQ_POLLIN, 0 }
            };

            zmq_poll(pollItems, sizeof(pollItems) / sizeof(zmq_pollitem_t), -1);

            if (pollItems[0].revents &= ZMQ_POLLIN) {
                forward(mFrontend.get(), mBackend.get());
            }
            if (pollItems[1].revents &= ZMQ_POLLIN) {
                forward(mBackend.get(), mFrontend.get());
            }
        }
    }

private:
    void forward(void *srcSocket, void *dstSocket)
    {
        zmq_msg_t routingIdMsg;
        zmq_msg_init(&routingIdMsg);
        // Read the routing message
        const int rc = zmq_msg_recv(&routingIdMsg, srcSocket, ZMQ_DONTWAIT);
        if (rc == -1) {
            if (errno == EAGAIN) {
                return;
            }
        }
        const std::string routingId{static_cast<const char *>(zmq_msg_data(&routingIdMsg)), zmq_msg_size(&routingIdMsg)};
        std::cout << "[SRV] Msg from router " << routingId << std::endl;

        readDelimiter(srcSocket);

        ensurePeerExists(srcSocket, routingId);
        int more = 0;
        size_t moreSize = sizeof(more);
        zmq_getsockopt(srcSocket, ZMQ_RCVMORE, &more, &moreSize);
        if (!more) {
            return;
        }

        // Forward the routing message
        zmq_msg_send(&routingIdMsg, dstSocket, ZMQ_SNDMORE);

        sendDelimiter(dstSocket, ZMQ_SNDMORE);

        // Forward the rest of the envelope
        more = 1;
        while (more) {
            zmq_msg_t msg;
            zmq_msg_init(&msg);
            zmq_msg_recv(&msg, srcSocket, 0);
            more = zmq_msg_more(&msg);
            zmq_msg_send(&msg, dstSocket, more ? ZMQ_SNDMORE : 0);
        }
    }

    void ensurePeerExists(void *srcSocket, const std::string &routingId)
    {
        if (srcSocket == mFrontend.get()) {
            auto conn = mConnections.find(routingId);
            if (conn == mConnections.end()) {
                const auto &[it, ok] = mConnections.insert({routingId, std::make_unique<Connection>(mCtx.get(), routingId)});
                mConnectionThreads.emplace_back(std::thread(&Connection::run, it->second.get()));

                // Read HELLO message
                zmq_msg_t msg;
                zmq_msg_init(&msg);
                zmq_msg_recv(&msg, srcSocket, 0);
                std::string hello{static_cast<const char*>(zmq_msg_data(&msg)), zmq_msg_size(&msg)};
                std::cout << "Reading hello message from " << routingId << ": '" << hello << "'" << std::endl;
            } else {
                //std::cout << "Peer for " << routingId << " already exists." << std::endl;
            }
        }
    }

    std::unique_ptr<void, ZmqContextDeleter> mCtx;
    std::unique_ptr<void, ZmqSocketDeleter> mFrontend;
    std::unique_ptr<void, ZmqSocketDeleter> mBackend;

    std::unordered_map<std::string, std::unique_ptr<Connection>> mConnections;
    std::thread mMonitorThread;
    std::vector<std::thread> mConnectionThreads;
};

int main(int argc, char **argv)
{
    Server server;
    server.run();

    return 0;
}
