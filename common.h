#pragma once

#include "zmq.h"

#include <cassert>
#include <iostream>

struct ZmqContextDeleter {
    void operator()(void *ctx) {
        zmq_ctx_term(ctx);
    }
};

struct ZmqSocketDeleter {
    void operator()(void *sock) {
        zmq_close(sock);
    }
};


void readDelimiter(void *socket)
{
    zmq_msg_t msg;
    zmq_msg_init(&msg);
    zmq_msg_recv(&msg, socket, 0);
    if (zmq_msg_size(&msg) > 0) {
        const std::string data{static_cast<const char *>(zmq_msg_data(&msg)), zmq_msg_size(&msg)};
        std::cerr << "Unexpected data when reading delimiter frame: " << data << std::endl;
    }
    assert(zmq_msg_size(&msg) == 0);
}

void sendDelimiter(void *socket, int flags = 0)
{
    zmq_msg_t msg;
    zmq_msg_init(&msg);
    zmq_msg_send(&msg, socket, flags);
}


